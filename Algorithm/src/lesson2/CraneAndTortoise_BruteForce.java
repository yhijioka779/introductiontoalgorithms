package lesson2;

import java.util.Scanner;

public class CraneAndTortoise_BruteForce {
	private static Scanner stdIn;

	public static void main(String[] args) {
		int sumOfAnimal; // 鶴と亀の合計
		int legNum; // 足の数
		int crane, turtle; // 鶴の数，亀の数
		stdIn = new Scanner(System.in);
		
		System.out.print("鶴と亀の合計: ");
		sumOfAnimal = stdIn.nextInt();
		System.out.print("足の数: ");
		legNum = stdIn.nextInt();
		
		for(crane = 1; crane <= sumOfAnimal; crane++){
			turtle = sumOfAnimal - crane;
			if (crane * 2 + turtle * 4 == sumOfAnimal){
				System.out.print("鶴の数: " + crane);
				System.out.println("亀の数: " + turtle);
				System.exit(0);
			}
		}
		
		System.out.println("足の数が" + legNum + "となる組み合わせは存在しません．");
	}
}
